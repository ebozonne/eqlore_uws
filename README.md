## EQLORE - urban weather station data - la Rochelle (France)

- weather station network: 1 rural (10 km from the city) and 7 urban weather stations in La Rochelle city
- réseau de stations météo : 1 station météo rurale (à 10 km de la ville) et 7 stations météo urbaines dans la ville de La Rochelle

### DATA
- See ./archives/ (in subfolders) for all weather station data (updated monthly) in CSV files (data grouped by year)
- Voir ./archives/ (dans les sous-dossiers) pour toutes les données des stations météorologiques (mises à jour mensuellement) dans des fichiers CSV (données regroupées par an)

### REFERENCE TO CITE WHEN USING DATA
Martinez, S., A. Machard, A. Pellegrino, K. Touili, L. Servant, et E. Bozonnet. « A Practical Approach to the Evaluation of Local Urban Overheating– A Coastal City Case-Study ». Energy and Buildings 253 (15 décembre 2021): 111522. https://doi.org/10.1016/j.enbuild.2021.111522.

### Locations and periods
- See ./config-station.xlsx for all weather station locations (lat,lon) and monitoring periods
- Voir ./config-station.xlsx pour tous les emplacements des stations météorologiques (lat, lon) et les périodes de mesure
