# ******************************************************************************
#  This file is part of eqlore.                                                 *
#                                                                              *
#  Copyright                                                                   *
#                                                                              *
#  eqlore is free software: you can redistribute it and/or modify               *
#  it under the terms of the GNU General Public License as published by        *
#  the Free Software Foundation, either version 3 of the License, or           *
#  (at your option) any later version.                                         *
#                                                                              *
#  eqlore is distributed in the hope that it will be useful,                    *
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
#  GNU General Public License for more details.                                *
#                                                                              *
#  You should have received a copy of the GNU General Public License           *
#  along with eqlore.  If not, see <https://www.gnu.org/licenses/>.             *
# ******************************************************************************
import folium
from folium.plugins import Draw
import shutil
import pandas as pd
import json
import branca.colormap as cmp
import seaborn
import pandas as pd
from folium.features import DivIcon
import commons as chP
from datetime import timedelta 
import matplotlib.pyplot as plt
from matplotlib.transforms import offset_copy
import matplotlib.patches as patches
import numpy as np
from matplotlib.lines import Line2D as Line



def get_station(date, lieu, infos_stations):
    infos_stations["testdebut"]=[x.days for x in date-infos_stations.debut]
    infos_stations["testfin"]=[x.days for x in infos_stations.fin-date]
    station = infos_stations[(infos_stations.localisation==lieu)&(infos_stations.testdebut>0)&(infos_stations.testfin>0)]
    return station.index[0].split('_')[0], station


def draw_my_map(debut, tableCorrespondance, infos_stations, dataStations):
    m = folium.Map(location=[46.155821282320524, -1.153276741101409], 
                   tiles='Stamen Toner',  zoom_start=12)


    tableCorrespondance = {
        'Vitesse_vent_moy' : '1min',
        'BattV_Avg' : '5min',
        'PTemp_C_Avg' : '5min',
        'Temp_moy' : '5min',
        'Hygro_moy' : '5min',
        'BattV_Min' : 'Battery',
        'Ray_solaire_moy' : '5min',
        'Ray_solaire_cumul' : '5min',
        'Ray_IR_moy' : '5min',
        'Ray_CGR3_Temp_C' : '5min',
        'Pluvio_cumul' : '5min',
        'Direction_vent_vecteur' : '10min', 
        'Vent_Diag' : '10Min'
    }

    debut_datetime = debut

    #----------------------------------------------------
    #  intervalle de calcul pour la moyenne
    #----------------------------------------------------
    nbr = 1
    fin = (debut_datetime + timedelta(hours=nbr)).strftime("%Y-%m-%d %H:%M:%S")
    debut = debut_datetime.strftime("%Y-%m-%d %H:%M:%S")
    #----------------------------------------------------
    #  Ajout des stations dans un dictionnaire
    #----------------------------------------------------
    liste_Des_Stations = []
    df = pd.DataFrame()
    for lieu in infos_stations['localisation'].dropna().drop_duplicates():
        liste_Des_Stations.append(get_station(debut_datetime, lieu, infos_stations)[0])
        df = pd.concat([df, get_station(debut_datetime, lieu, infos_stations)[1]], axis = 0)
    df.index = [x.split('_')[0] for x in df.index]
    #----------------------------------------------------
    #  Renommer
    #----------------------------------------------------
    liste_temp = []
    delta_temp = []
    color = []
    for name in df.index:
        try :liste_temp.append(round(dataStations[name][tableCorrespondance['Temp_moy']][debut:fin].Temp_moy.mean(),2))
        except : print(f'no data > {name}')
        try :
            dt = dataStations[name][tableCorrespondance['Temp_moy']][debut:fin].Temp_moy.mean()\
                              -dataStations['Station-3A'][tableCorrespondance['Temp_moy']][debut:fin].Temp_moy.mean()
            delta_temp.append(dt)
            
            #bricolage pour la couleur
            if dt > 0:
                color.append('red')
            elif dt<0:
                color.append('cyan')
            elif dt ==0:
                color.append('black')
        except :print(f'no data > {name}')
            
            
        
    df['temperature'] = liste_temp
    df['dt'] = delta_temp
    df['color'] = color
    df['round-dt'] = round(df['dt'],2)

    #----------------------------------------------------
    #  Ajout des stations dans un dictionnaire
    #----------------------------------------------------

    text = 'test'
    for i in range(0,len(df)):
       folium.Circle(
          location=[df.iloc[i]['lat'], df.iloc[i]['lon']],
          popup=df.iloc[i].index,
          radius= max(400*abs(df.iloc[i]['dt']),10),
          color=df.iloc[i].color,
          fill=True,
          fill_color=df.iloc[i].color,
            fill_opacity=0.6,
            stroke = False
       ).add_to(m)

    for i in range(0,len(df)):
        folium.map.Marker(
            location=[df.iloc[i]['lat'], df.iloc[i]['lon']],
            icon=DivIcon(
                icon_size=(150,10),
                icon_anchor=(0,25),
                html='<div style="font-size: 16pt">%s</div>' % df.iloc[i].name
                )
            ).add_to(m)
        
        folium.map.Marker(
            location=[df.iloc[i]['lat'], df.iloc[i]['lon']],
            icon=DivIcon(
                icon_size=(150,10),
                icon_anchor=(0,0),
                html='<div style="font-size: 16pt">%s °C</div>' % df.iloc[i]['round-dt']
                )
            ).add_to(m)

    return m

def tableCorrespondance():
    tableCorrespondance = {
    'Vitesse_vent_moy' : '1min',
    'BattV_Avg' : '5min',
    'PTemp_C_Avg' : '5min',
    'Temp_moy' : '5min',
    'Hygro_moy' : '5min',
    'BattV_Min' : 'Battery',
    'Ray_solaire_moy' : '5min',
    'Ray_solaire_cumul' : '5min',
    'Ray_IR_moy' : '5min',
    'Ray_CGR3_Temp_C' : '5min',
    'Pluvio_cumul' : '5min',
    'Direction_vent_vecteur' : '10min', 
    'Vent_Diag' : '10Min'
        }
    return tableCorrespondance

def correctionData(listDesStations, dataStations):
    #----------------------------------------------------
    #  modification des données solaires
    #----------------------------------------------------
    tableCorrespondance = {
        'Vitesse_vent_moy' : '1min',
        'BattV_Avg' : '5min',
        'PTemp_C_Avg' : '5min',
        'Temp_moy' : '5min',
        'Hygro_moy' : '5min',
        'BattV_Min' : 'Battery',
        'Ray_solaire_moy' : '5min',
        'Ray_solaire_cumul' : '5min',
        'Ray_IR_moy' : '5min',
        'Ray_CGR3_Temp_C' : '5min',
        'Pluvio_cumul' : '5min',
        'Direction_vent_vecteur' : '10min', 
        'Vent_Diag' : '10Min'
    }
    config = pd.read_excel('config-stations.xlsx', index_col=0) 
    for station in listDesStations:
        try:
            CMP3 = dataStations[station][tableCorrespondance['Ray_solaire_moy']]['Ray_solaire_moy']
            dataStations[station][tableCorrespondance['Ray_solaire_moy']]['Ray_solaire_moy'] = CMP3*(15.02/config['CMP3'][station])
        except:pass
        try:
            CGR3 = dataStations[station][tableCorrespondance['Ray_IR_moy']]['Ray_IR_moy']
            dataStations[station][tableCorrespondance['Ray_IR_moy']]['Ray_IR_moy']= CGR3*((1000/config['CGR3'][station])/17)
        except:pass
    return dataStations, tableCorrespondance


def smartPlot(listeStation, variable, debut, fin,  dataStations, tableCorrespondance):
    import bokeh.io
    from bokeh.palettes import Paired as palette
    from bokeh.plotting import figure, output_file, show, ColumnDataSource
    from bokeh.palettes import magma, plasma, viridis,  cividis, Purples
    color = palette[len(listeStation)]
    p = figure(plot_width=800, plot_height=250, x_axis_type="datetime", title = variable)
    for station, col in zip(listeStation, color):
        try :
            df = dataStations[station][tableCorrespondance[variable]][debut:fin]
            df['date'] = pd.to_datetime(df.index)
            p.line(df['date'], df[variable], color = col, alpha=0.5, legend = station)
        except:pass
    show(p)

def battery(dataStations, debut, present):
    from datetime import timedelta 
    batt = 'BattV_Avg'
    DATA_check = {}
    debut = present.replace(minute = 0, second = 0, microsecond = 0)
    finish = debut+timedelta(days=1)
    debut = debut.strftime("%Y-%m-%d %H:%M:%S")
    finish = finish.strftime("%Y-%m-%d %H:%M:%S")
    for name in dataStations:
        try:
            DATA_check[name] = dataStations[name][batt][debut:finish]
            mini = DATA_check[name].min()
            if mini<10:
                print('LA BATTERIE ' + name + ' EST DECHARGEE')
            if len(DATA_check[name]) == 0:
                pass
            else:
                print(name)
                print('BATTERIE OK')
        except:pass



def mapLR(dataStations, debut, listDesStations, tableCorrespondace):
    """
    =======================
    Map
    =======================

    Pour installer cartopy : conda install --channel conda-forge cartopy

    """
    import choosePlot as chP
    from datetime import datetime
    from datetime import timedelta 
    import matplotlib.pyplot as plt
    from matplotlib.transforms import offset_copy
    import matplotlib.patches as patches
    import cartopy.crs as ccrs
    import numpy as np
    from matplotlib.lines import Line2D as Line
    #----------------------------------------------------
    # Useful for colorbar
    #----------------------------------------------------
    import cartopy.io.img_tiles as cimgt
    from mpl_toolkits.axes_grid1 import AxesGrid
    from cartopy.mpl.geoaxes import GeoAxes
    stamen_terrain = cimgt.Stamen("terrain")
    projection = stamen_terrain.crs
    axes_class = (GeoAxes, dict(map_projection=projection))

    # Create a GeoAxes in the tile's projection.
    fig = plt.figure(figsize=(20, 10))
    axgr = AxesGrid(fig, 111, axes_class = axes_class,
                        nrows_ncols = (1, 1),
                        axes_pad = 0,
                        cbar_location='right',
                        cbar_mode='single',
                        cbar_size='0%',
                        label_mode='')  # note the empty label_mode
    #----------------------------------------------------
    #  intervalle de calcul pour la moyenne
    #----------------------------------------------------
    nbr = 1
    fin = (debut + timedelta(hours=nbr)).strftime("%Y-%m-%d %H:%M:%S")
    debut = debut.strftime("%Y-%m-%d %H:%M:%S")
    
    #----------------------------------------------------
    #  Ajout des stations dans un dictionnaire
    #----------------------------------------------------
    dataTemp = {}
    for station in listDesStations:
        dataTemp[station] = dataStations[station][tableCorrespondace['Temp_moy']]
    dataPos = [
        [-1.158845984057876, 46.14496400302767], # station 1A
        [],                                    # station 1B      
        [-1.146211345412327, 46.15113874727135], # station 1C
        [-1.155870285076597, 46.16869150693134], # station 1D
        [-1.155348489467828, 46.168147091533314],# station 1E
        [-1.219480000000000, 46.175516000000000],                  # station 2A
        [-1.148074409034999, 46.167916208017544],# station 2B
        [],                                    # station 2C
        [],                                    # station 2D
        [-1.030987516728151, 46.203311177306006],# station 3A
        [-1.157541323896073, 46.14649249701438], # station 3B
        [],                                    # station 3C
        []                                     # station 3D
    ]
    dataStationOffset = [
        [0, -25], # station 1A
        [],       # station 1B      
        [+75, 0], # station 1C
        [-25, 0], # station 1D
        [+25, +25],# station 1E
        [-25, 0],# station 2A
        [+75, 0],# station 2B
        [],                                    # station 2C
        [],                                    # station 2D
        [-25, 0],# station 3A
        [-25, 0], # station 3B
        [],                                    # station 3C
        []                                     # station 3D
    ]
    # stations
    dataMap = {}
    for name, pos, offsetStation in zip(listDesStations, dataPos, dataStationOffset):
        try:
            dataMap[name] = {}
            dataMap[name]['x']  = pos[0]
            dataMap[name]['y']  = pos[1]
            dataMap[name]['xOffset'] = offsetStation[0]
            dataMap[name]['yOffset'] = offsetStation[1]
        except:pass
        try :    
            dataMap[name]['temp'] = round(dataStations[name][tableCorrespondace['Temp_moy']][debut:fin].Temp_moy.mean(),2)
        except : dataMap[name]['temp'] = -1000
     #---------------------------------------
     # données supplémentaires
     #---------------------------------------
    listStationSupp = ['tipee', 'aeroport']
    positionStationSupp = { 'tipee' : [-1.146292219298613, 46.18446508428363],
                            'aeroport' : [-1.193071998751281, 46.17798777969875]
    }
    for station in listStationSupp:
        dataMap[station] = {}
        try:
            dataMap[station]['x']  = positionStationSupp[station][0]
            dataMap[station]['y']  = positionStationSupp[station][0]
        except:pass
        try :dataMap[station]['temp'] = round(dataStations[name][debut:fin].Temp_moy.mean(),2)
        except : dataMap[station]['temp'] = -1000
    #----------------------------------------------------
    #  Création du plot
    #----------------------------------------------------    
    # Create a GeoAxes in the tile's projection.
    ax = axgr.axes_all[0]
    # Limit the extent of the map to a small longitude/latitude range.
    ax.set_extent([-1.3, -1, 46.1, 46.22], ccrs.Geodetic())
    # Add the Stamen data at zoom level 8.
    ax.add_image(stamen_terrain, 12, alpha = 1)
    #----------------------------------------------------
    # Plot de chaque station
    #---------------------------------------------------- 
    # Use the cartopy interface to create a matplotlib transform object
    # for the Geodetic coordinate system. We will use this along with
    # matplotlib's offset_copy function to define a coordinate system which
    geodetic_transform = ccrs.Geodetic()._as_mpl_transform(ax)
    text_transform = offset_copy(geodetic_transform, units='dots', x=-25)
    text_transformRight = offset_copy(geodetic_transform, units='dots', x=+25)
    fontsize = 12
    alpha = 0.3
    alphaTemp = 0.8
    valRur = dataMap['Station-3A']['temp']
    #----------------------------------------------------
    # Températures
    #----------------------------------------------------
    def mapStation(ax,name,Trural,Tstation,xStation,yStation,alphaTemp=0.8,xOffset=0,yOffset=0,rural=False):
        if not rural: color, markerSize = chP.f(Trural, Tstation)
        if rural: color,markerSize='k',6
        ax.plot(xStation, yStation, marker='o', color=color, markersize=markerSize, markeredgecolor = 'k',
                alpha=alphaTemp, transform=ccrs.Geodetic())
        ax.text(xStation, yStation, name+'\n{} °C'.format(Tstation),
                verticalalignment = 'center', horizontalalignment='right',
                transform = offset_copy(geodetic_transform, units='dots', x = xOffset, y = yOffset), fontsize = 11,
                bbox = dict(facecolor='gray', alpha = alpha, boxstyle='round'))
        return()
    for name in listDesStations:
        rural = False
        if name == 'Station-3A': rural = True
        try: mapStation(ax,name,valRur,dataMap[name]['temp'],dataMap[name]['x'], dataMap[name]['y'],
                   xOffset=dataMap[name]['xOffset'],yOffset=dataMap[name]['yOffset'],rural=rural)
        except:pass
        #if name == 'Station-2A': print('station2A ici')
        #if name == 'Station-3A': print('station3A ici') 
        # if name == 'Station-3A':
            # ax.plot(dataMap[name]['x'], dataMap[name]['y'], marker='o',color='k', markersize=markerSize, markeredgecolor = 'k',
                    # alpha= 1, transform=ccrs.Geodetic())
            # ax.text(dataMap[name]['x'], dataMap[name]['y'], name+'\n{} °C'.format(dataMap[name]['temp']),
                    # verticalalignment = 'center', horizontalalignment='right',
                    # transform = offset_copy(geodetic_transform, units='dots', x = -25), fontsize = 11,
                    # bbox = dict(facecolor='white', alpha = alpha, boxstyle='round', edgecolor = 'k'))
     
        # elif name == 'Station-1D':
            # ax.plot(dataMap[name]['x'], dataMap[name]['y'], marker='o',color=color, markersize=6, markeredgecolor = 'k',
                # alpha= 1, transform=ccrs.Geodetic())
            # ax.text(dataMap[name]['x'], dataMap[name]['y'], name+'\n{} °C'.format(dataMap[name]['temp']),
                # verticalalignment = 'center', horizontalalignment='right',
                # transform = offset_copy(geodetic_transform, units='dots', x = -25), fontsize = 11,
                # bbox = dict(facecolor='gray', alpha = alpha, boxstyle='round'))
        # """---------------------------------------------------- 
         # On décale les stations prochent
        # ----------------------------------------------------"""
        # elif name == 'Station-1C':
            # color, markerSize = chP.f(valRur, dataMap[name]['temp'])
            # ax.plot(dataMap[name]['x'], dataMap[name]['y'], marker='o', color=color, markersize=markerSize,markeredgecolor = 'k',
                # alpha=alphaTemp, transform=ccrs.Geodetic())
            # ax.text(dataMap[name]['x'], dataMap[name]['y'], name+'\n{} °C'.format(dataMap[name]['temp']),
                # verticalalignment = 'center', horizontalalignment='right',
                # transform = offset_copy(geodetic_transform, units='dots', x = +75), fontsize = 11,
                # bbox = dict(facecolor='gray', alpha = alpha, boxstyle='round'))
        
        # elif name == 'Station-1E':
            # color, markerSize = chP.f(valRur, dataMap[name]['temp'])
            # ax.plot(dataMap[name]['x'], dataMap[name]['y'], marker='o', color=color, markersize=markerSize,markeredgecolor = 'k',
                # alpha=alphaTemp, transform=ccrs.Geodetic())
            # ax.text(dataMap[name]['x'], dataMap[name]['y'], name+'\n{} °C'.format(dataMap[name]['temp']),
                # verticalalignment = 'center', horizontalalignment='right',
                # transform = offset_copy(geodetic_transform, units='dots', x = +25, y = +25), fontsize = 11,
                # bbox = dict(facecolor='gray', alpha = alpha, boxstyle='round'))
            
        # elif name == 'Station-2B':
            # color, markerSize = chP.f(valRur, dataMap[name]['temp'])
            # ax.plot(dataMap[name]['x'], dataMap[name]['y'], marker='o', color=color, markersize=markerSize,markeredgecolor = 'k',
                # alpha=alphaTemp, transform=ccrs.Geodetic())
            # ax.text(dataMap[name]['x'], dataMap[name]['y'], name+'\n{} °C'.format(dataMap[name]['temp']),
                # verticalalignment = 'center', horizontalalignment='right',
                # transform = offset_copy(geodetic_transform, units='dots', x = +75), fontsize = 11,
                # bbox = dict(facecolor='gray', alpha = alpha, boxstyle='round'))
            
        # elif name == 'Station-1A':
            # color, markerSize = chP.f(valRur, dataMap[name]['temp'])
            # ax.plot(dataMap[name]['x'], dataMap[name]['y'], marker='o', color=color, markersize=markerSize,markeredgecolor = 'k',
                # alpha=alphaTemp, transform=ccrs.Geodetic())
            # ax.text(dataMap[name]['x'], dataMap[name]['y'], name+'\n{0} °C'.format(dataMap[name]['temp']),
                # verticalalignment = 'center', horizontalalignment='right',
                # transform = offset_copy(geodetic_transform, units='dots', x = 0, y = -25), fontsize = 11,
                # bbox = dict(facecolor='gray', alpha = alpha, boxstyle='round'))
        # """----------------------------------------------------"""
        # else :
            # try:
                # color, markerSize = chP.f(valRur, dataMap[name]['temp'])
                # ax.plot(dataMap[name]['x'], dataMap[name]['y'], marker='o', color=color, markersize = markerSize, markeredgecolor = 'k',
                                            # alpha=alphaTemp, transform=ccrs.Geodetic())
                # #text
                # ax.text(dataMap[name]['x'], dataMap[name]['y'], name+'\n{} °C'.format(dataMap[name]['temp']),
                                            # verticalalignment = 'center', horizontalalignment='right',
                                            # transform = offset_copy(geodetic_transform, units='dots', x = -25), fontsize = 11,
                                            # bbox = dict(facecolor='gray', alpha = alpha, boxstyle='round'))
            # except:pass
    #----------------------------------------------------
    # Useful for colorbar
    #----------------------------------------------------
    valX = np.arange(1, 40, 1)
    valY = np.arange(1, 40, 1)
    data = np.random.randint(-10,45,(len(valX), len(valY)))
    p = ax.contourf(valX, valY, data,
                             40,
                             transform=projection,
                             center = 0,
                             vmin = -5,
                             vmax = 45,
                             cmap='coolwarm')
    axgr.cbar_axes[0].colorbar(p)
    #----------------------------------------------------
    # Useful for absolute temp - thermometer
    #----------------------------------------------------
    h = 46.1 + ((dataMap['Station-3A']['temp']+5)/50)*(46.22-46.1)
    ax.plot(-1.003-0.003, h, marker=">", color='k', markersize=15,markeredgecolor = 'k', alpha=1, transform=ccrs.Geodetic())
#     #----------------------------------------------------
#     # Useful for rectangle
#     #---------------------------------------------------
#     rectBase = patches.Rectangle((-1.005,46.1), 0.005, 0.12, linewidth=1, edgecolor='white', facecolor='white', transform=ccrs.Geodetic())
#     rectTemp = patches.Rectangle((-1.005,46.1), 0.005, h-46.1, linewidth=1, edgecolor='black', facecolor='black', transform=ccrs.Geodetic())
#     ax.add_patch(rectBase)
#     ax.add_patch(rectTemp)
#     #----------------------------------------------------
#     # Text
#     #---------------------------------------------------
    ax.text(-1.003-0.009, h, 'Station rurale'+'\n{} °C'.format(dataMap['Station-3A']['temp']),
                verticalalignment = 'center', horizontalalignment='right', fontSize = 14, transform=ccrs.Geodetic())

    #----------------------------------------------------
    # Useful for legend
    #----------------------------------------------------
    legend_artists = [Line([], [], color='None', marker = 'o',markeredgecolor = 'k', markerSize = size, linestyle = 'None')
                          for size in (6, 12, 18, 24, 30, 36, 42)]
    legend_texts = ['$\Delta T$ = 0 °C', '$\Delta T$ = 1 °C', '$\Delta T$ = 2 °C', '$\Delta T$ = 3 °C', '$\Delta T$ = 4 °C', '$\Delta T$ = 5 °C', '$\Delta T$ = 6 °C']
    legend = ax.legend(legend_artists, legend_texts, fancybox=False,
                            loc='upper left', framealpha=0.5, prop={'size': 21})
    plt.show()













pal = seaborn.color_palette(palette= "coolwarm", n_colors= 10, desat= 1)
def f(valRur, valUrb):
    if valRur-valUrb>=100:
        color = 'k'
        markerSize = 6
    elif valRur-valUrb>=7:
        color = pal[0]
        markerSize = 42
    elif valRur-valUrb>=6:
        color = pal[0]
        markerSize = 36
    elif valRur-valUrb>=5:
        color = pal[0]
        markerSize = 30
    elif valRur-valUrb>=4:
        color = pal[1]
        markerSize = 24
    elif valRur-valUrb>=3:
        color = pal[2]
        markerSize = 18
    elif valRur-valUrb>=2:
        color = pal[3]
        markerSize = 12
    elif valRur-valUrb>=1:
        color = pal[4]
        markerSize = 6
    elif valRur-valUrb>=0:
        color = pal[5]
        markerSize = 6
    elif valRur-valUrb>=-1:
        color = pal[6]
        markerSize = 12
    elif valRur-valUrb>=-2:
        color = pal[7]
        markerSize = 18
    elif valRur-valUrb>=-3:
        color = pal[8]
        markerSize = 24
    elif valRur-valUrb>=-4:
        color = pal[9]
        markerSize = 30
    else:
        color = 'k'
        markerSize = 6
    return color, markerSize

def run(name_geojson = 'mask.geojson',
        lat = 46.18464,
        lon = -1.14642,
        zoom = 14
        ):
    """
    Fonction qui petmet de récupérer les données d'une bbox tracé sur notebook

    :param name_geojson: nom du fichier sauvegardé dans
    :return: le fichier .geojson dans le dossier data
    """
    m = folium.Map(location=[lat, lon], zoom_start=zoom)
    
    data = "./geoclimate/rsu_lcz.geojson"
    with open(data) as f:
        myGeoJsonClimate = json.load(f)

    tableCorresp = {   
        1 :['LCZ 1: Compact high-rise', '#8b0101'],
        2 :['LCZ 2: Compact mid-rise', '#cc0200'],
        3 :['LCZ 3: Compact low-rise', '#fc0001'],
        4 :['LCZ 4: Open high-rise', '#be4c03'],
        5 :['LCZ 5: Open mid-rise', '#ff6602'],
        6 :['LCZ 6: Open low-rise', '#ff9856'],
        7 :['LCZ 7: Lightweight low-rise', '#fbed08'],
        8 :['LCZ 8: Large low-rise', '#bcbcba'],
        9 :['LCZ 9: Sparsely built', '#ffcca7'],
        10 :['LCZ 10: Heavy industry', '#57555a'],
        101 :['LCZ A: Dense trees', '#006700'],
        102 :['LCZ B: Scattered trees', '#05aa05'],
        103 :['LCZ C: Bush,scrub', '#648423'],
        104 :['LCZ D: Low plants', '#bbdb7a'],
        105 :['LCZ E: Bare rock or paved', '#010101'],
        106 :['LCZ F: Bare soil or sand', '#fdf6ae'],
        107 :['LCZ G: Water', '#6d67fd']
    }
    for k, item in enumerate(myGeoJsonClimate['features']):
        try:myGeoJsonClimate['features'][k]['properties']['legend'] = tableCorresp[myGeoJsonClimate['features'][k]['properties']['LCZ_PRIMARY']][0]
        except:myGeoJsonClimate['features'][k]['properties']['legend'] = tableCorresp[int(str(myGeoJsonClimate['features'][k]['properties']['LCZ_PRIMARY']).replace('0',''))][0]
        
    for x in myGeoJsonClimate['features']:
        x['LCZ_PRIMARY'] = x['properties']['LCZ_PRIMARY']
    #     if x['properties']['LCZ_PRIMARY'] in [101, 102, 103, 104, 105, 106, 107]:
    #         x['LCZ_PRIMARY'] = int(str(x['properties']['LCZ_PRIMARY']).replace('0',''))
    #     else:
    #         x['LCZ_PRIMARY'] = x['properties']['LCZ_PRIMARY']

    step = cmp.StepColormap(
     [tableCorresp[k][1] for k in tableCorresp.keys()],
     vmin=0, vmax=18,
     index=tableCorresp.keys(),  #for change in the colors, not used fr linear
     caption='Color Scale for Map'    #Caption for Color scale or Legend
    )


    folium.GeoJson(
            myGeoJsonClimate,
            style_function=lambda feature: {
                'fillColor': step(feature['LCZ_PRIMARY']),
                'color': 'black',
                'weight': 0.5,
                'alpha': 1,
                "fillOpacity": 0.35},
            #         'dashArray' : '5, 5'
            tooltip=folium.features.GeoJsonTooltip(
                fields=['legend'],
                aliases=[''],
            )

        ).add_to(m)

    title_html = '''
                 <h3 align="center" style="font-size:20px"><b>Cartographie - LCZ La Rochelle</b></h3>
                 '''
    m.get_root().html.add_child(folium.Element(title_html))
    
    
    
    Draw(
        export=True,
        filename=name_geojson,
        position='topleft',
        draw_options={'polyline': {'allowIntersection': False}},
        edit_options={'poly': {'allowIntersection': False}}
    ).add_to(m)

    return m
    

class header():

    def __init__(self, epwPath = 'FRA_Paris.Orly.071490_IWEC.epw'):
        dat_txt = open(epwPath, "r")
        myList = dat_txt.readlines()
        dat_txt.close()
        self.head = myList[:7]

        

def get():
    columns = ['Year', 'Month', 'Day', 'Hour', 'Minute',
           'Data Source and Uncertainty Flags', 'Dry Bulb Temperature',
           'Dew Point Temperature', 'Relative Humidity',
           'Atmospheric Station Pressure', 'Extraterrestrial Horizontal Radiation',
           'Extraterrestrial Direct Normal Radiation',
           'Horizontal Infrared Radiation Intensity',
           'Global Horizontal Radiation', 'Direct Normal Radiation',
           'Diffuse Horizontal Radiation', 'Global Horizontal Illuminance',
           'Direct Normal Illuminance', 'Diffuse Horizontal Illuminance',
           'Zenith Luminance', 'Wind Direction', 'Wind Speed', 'Total Sky Cover',
           'Opaque Sky Cover (used if Horizontal IR Intensity missing)',
           'Visibility', 'Ceiling Height', 'Present Weather Observation',
           'Present Weather Codes', 'Precipitable Water', 'Aerosol Optical Depth',
           'Snow Depth', 'Days Since Last Snowfall', 'Albedo',
           'Liquid Precipitation Depth', 'Liquid Precipitation Quantity', '']
    return columns



def index():
    initRun = '2021-01-01 01:00:00'
    endRun = '2022-01-01 00:00:00'
    myIndex = pd.date_range(start=initRun, end=endRun, freq='1h')
    return myIndex