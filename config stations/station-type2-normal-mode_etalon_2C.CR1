'W/O:  CCSF005437_2
'Customer:  JD Environnement
'Date:  16th January 2019
'Campbell Scientific Contact:  Vanessa Ben Tahar
'
'Program used to test your weather station.
'Please note the following important information:
'Use the test program as a starting point, you will need to adapt it to suit your own application.
'
'
'-Wiring For CR1000-
'
'  HC2AS3 (constant power)
'    1H:  Brown
'    1L:  Yellow
'    2H:  White
'    2L:  Link to 1L
'    G:  Grey
'    G:  Clear
'    12V:  Green
'
'  CMP3 Pyranometer (CSL)
'    3H:  Red
'    3L:  Blue
'    3L:  Link To AG
'    AG:  Clear
'
'  CGR3 Atmospheric infra-red radiation
'  Pyrgeometer
'    4H:  Red
'    4L:  Blue
'    4L:  Link to AG
'  PT100
'    5L:  Yellow (via 4WPB100)
'    AG:  Green (via 4WPB100)
'    6H:  Brown
'    6L:  Grey
'    VX1: 4WPB100 (Black)
'
'  014A Wind Speed Sensor
'    AG:  Clear
'    AG:  White
'    P1:  Black
'
'-Measurement Labels-

  'CMP3 Pyranometer sensor calibration μV/Wm^2:
  Const CMP3_cal=15.02

'GPRS modem information
'Const ModemPort = ComSDC7  'This is the port to which the modem is attached, typically ComSDC7 via an SC105.
  Const ModemPort = ComRS232
  Const Modembaud = 115200   'Modem baud rate
  Const ModemOnMinutes = 10  'Modem switched on for ModemOnMinutes every hour
  Public ModemFlag As Boolean

'Declare Variables and Units
	Public BattV : Units BattV = Volts
  Public PTemp_C : Units PTemp_C = Deg C
  Public HC2AS3_AirTC : Units HC2AS3_AirTC = Deg C
  Public HC2AS3_RH : Units HC2AS3_RH = %
	Public CMP3_Solar_Wm2 : Units CMP3_Solar_Wm2 = W/m²
	Public CMP3_Solar_MJ : Units CMP3_Solar_MJ = MJ/m²
	Public CGR3_Wm2 : Units CGR3_Wm2 = W/m²
	Public CGR3_Temp_C : Units CGR3_Temp_C = Deg C
  Public W014A_WS_ms : Units W014A_WS_ms = meters / sec
	
'Define Data Tables
DataTable(Table_5Min,True,-1)
  DataInterval(0,5,Min,10)
  Average(1,BattV,FP2,False)
  Average(1,PTemp_C,FP2,False)
	Average(1,HC2AS3_AirTC,FP2,False) : FieldNames("Temp_moy") 
  Average(1,HC2AS3_RH,FP2,False): FieldNames("Hygro_moy")
  Average(1,CMP3_Solar_Wm2,FP2,False) : FieldNames("Ray_solaire_moy")
  Totalize(1,CMP3_Solar_MJ,IEEE4,False) : FieldNames("Ray_solaire_cumul")
  Average(1,CGR3_Wm2,FP2,False) : FieldNames("Ray_IR_moy")
EndTable

DataTable(Table_1Min,True,-1)
  DataInterval(0,1,Min,10)
	Average(1,W014A_WS_ms,FP2,False) : FieldNames("Vitesse_vent_moy")
EndTable

DataTable(Table_Battery,True,-1)
  DataInterval(0,1440,Min,10)
  Minimum(1,BattV,FP2,False,False)
EndTable

'Main Program
BeginProg
  'In case the modem was connected when the station was reset
  PPPClose
  
  'Main Scan
  Scan(15,Sec,1,0)

    'Default CR1000 Datalogger Battery Voltage measurement 'BattV'
    Battery(BattV)
    'Default CR1000 Datalogger Wiring Panel Temperature measurement 'PTemp_C'
    PanelTemp(PTemp_C,_50Hz)

    If IfTime (0,60,Sec)
    'HC2AS3 (constant power) Temperature & Relative Humidity Sensor measurements 'AirTC' and 'RH'
  			VoltDiff(HC2AS3_AirTC,1,mV2500,1,True,0,_50Hz,0.1,-40)
			HC2AS3_AirTC = 1.00001407 * HC2AS3_AirTC + 0.09153847
      	VoltDiff(HC2AS3_RH,1,mV2500,2,True,0,_50Hz,0.1,0)
      	If HC2AS3_RH > 100 AND HC2AS3_RH < 103 Then HC2AS3_RH = 100
    End If

		If IfTime(0,30,Sec)
        'CMP3 Pyranometer measurement in 'W/m²' and 'SlrMJ'
        VoltDiff(CMP3_Solar_Wm2,1,mV25,3,True,0,_50Hz,1000 / CMP3_cal,0)
        'Set negative readings to zero:
        If CMP3_Solar_Wm2 < 0 Then CMP3_Solar_Wm2=0
        CMP3_Solar_MJ = CMP3_Solar_Wm2 * (15 * 0.000001)
    
        'CGR3 Atmospheric infra-red radiation
        VoltDiff(CGR3_Wm2,1,AutoRange,4,True,0,_50Hz,17,0)
     	'PT100 PRT Temperature Probe (4WPB100) (CSL) measurement 'Temp_C'
		BrHalf4W(CGR3_Temp_C,1,mV25,mV25,5,Vx1,1,2035,True,True,0,_50Hz,1,0)
		PRT(CGR3_Temp_C,1,CGR3_Temp_C,1,0)   
		EndIf
		
    '014A Wind Speed Sensor measurement 'WS_ms'
    PulseCount(W014A_WS_ms,1,1,2,1,0.8,0.447)
    If W014A_WS_ms < 0.457 Then W014A_WS_ms = 0
  
    'Modem control sequence
    If TimeIntoInterval (0,60,Min) Then   'Every hour turn on the modem
      ModemFlag = True 'Power on modem
      Delay (1,2,sec)
      PPPOpen
    EndIf
    If TimeIntoInterval (ModemOnMinutes,60,Min) Then  'After ModemOnMinutes turn modem off
      PPPClose
      SerialOpen(ModemPort,Modembaud,0,0,100)
      'Send the command to log off the network and shutdown cleanly, wait up to 3 sec.
      SerialOut(ModemPort,"+++","0" + CHR(13),1,150) 'Send the +++ sequence to get modem in command mode
      SerialOut(ModemPort,"AT+CFUN=0" + CHR(13),"0" + CHR(13),1,300) 'Clean shutdown
      Delay (1,2,sec) 'Delay to allow deregistration
      SerialClose(ModemPort)
      ModemFlag = False
    EndIf
    SW12 (ModemFlag)

    'Call Data Tables and Store Data
    CallTable Table_5Min
    CallTable Table_1Min
    CallTable Table_Battery
  NextScan
EndProg
