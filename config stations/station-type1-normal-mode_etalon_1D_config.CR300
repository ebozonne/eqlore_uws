'W/O:  CCSF005437_1
'Customer:  JD Environnement
'Date:  16th January 2019
'Campbell Scientific Contact:  Vanessa Ben Tahar
'
'Program used to test your weather station.
'Please note the following important information:
'Use the test program as a starting point, you will need to adapt it to suit your own application.
'
'
'-Wiring for CR300 Series-
'
'  HC2AS3 (constant power)
'    BAT +:  Green
'    1H:  Brown
'    2H:  White
'    1L:  Yellow
'    1L:  link to 2L
'    G:  Grey
'    G:  Clear
'
'  014A Wind Speed Sensor
'    G:  White
'    G:  Clear
'    P_SW:  Black
'
'  GSM Transceiver Control (CSL)
'    SW12V:  Red
'    G:  Black
'
'-Measurement Labels-

'GPRS modem information
'Const ModemPort = ComSDC7  'This is the port to which the modem is attached, typically ComSDC7 via an SC105.
Const ModemPort = ComRS232
Const Modembaud = 115200   'Modem baud rate
Const ModemOnMinutes = 10  'Modem switched on for ModemOnMinutes every hour
Public ModemFlag As Boolean

'Declare Variables and Units
Public BattV : Units BattV = Volts
Public PTemp_C : Units PTemp_C = Deg C
Public HC2AS3_AirTC : Units HC2AS3_AirTC = Deg C
Public HC2AS3_RH : Units HC2AS3_RH = %
Public W014A_WS_ms : Units W014A_WS_ms = meters/sec

'Define Data Tables
DataTable(Table_15Sec,True,-1)
  DataInterval(0,15,Sec,10)
  Average(1,BattV,FP2,False)
  Average(1,PTemp_C,FP2,False)
  Average(1,HC2AS3_AirTC,FP2,False) : FieldNames("Temp_moy")
  Average(1,HC2AS3_RH,FP2,False) : FieldNames("Hygro_moy")
EndTable

DataTable(Table_1min,True,-1)
  DataInterval(0,1,Min,10)
  Average(1,W014A_WS_ms,FP2,False) : FieldNames("Vitesse_vent_moy")
EndTable

DataTable(Table_Battery,True,-1)
  DataInterval(0,1440,Min,10)
  Minimum(1,BattV,FP2,False,False)
EndTable

'Main Program
BeginProg
  'In case the station was reset while being connected
  PPPClose

  'Main Scan
  Scan(15,Sec,1,0)

    'Default CR300 Datalogger Battery Voltage measurement 'BattV'
    Battery(BattV)
    'Default CR300 Datalogger Processor Temperature measurement 'PTemp_C'
    PanelTemp(PTemp_C,50)

    If TimeIntoInterval (0,60,Sec)
      'HC2AS3 (constant power) Temperature & Relative Humidity Sensor measurements 'AirTC' and 'RH'
      VoltDiff(HC2AS3_AirTC,1,mV2500,1,True,0,_50Hz,0.1,-40)
	  HC2AS3_AirTC = 1.00163885 * HC2AS3_AirTC - 0.0619242 
      VoltDiff(HC2AS3_RH,1,mV2500,2,True,0,_50Hz,0.1,0)
	  HC2AS3_RH = 1.0275 * HC2AS3_RH - 1.1864
      If HC2AS3_RH > 100 AND HC2AS3_RH < 103 Then HC2AS3_RH = 100
    End If

    '014A Wind Speed Sensor measurement 'WS_ms'
    PulseCount(W014A_WS_ms,1,P_SW,2,1,0.8,0.447)
    If W014A_WS_ms < 0.457 Then W014A_WS_ms = 0

    'Modem control sequence
    If TimeIntoInterval (0,60,Min) Then   'Every 1 hour turn on the modem
      ModemFlag = True 'Power on modem
      Delay (1,2,sec)
      PPPOpen
    EndIf
    If TimeIntoInterval (ModemOnMinutes,60,Min) Then  'After ModemOnMinutes turn modem off
      PPPClose
      SerialOpen(ModemPort,Modembaud,0,0,100)
      'Send the command to log off the network and shutdown cleanly, wait up to 3 sec.
      SerialOut(ModemPort,"+++","0" + CHR(13),1,150) 'Send the +++ sequence to get modem in command mode
      SerialOut(ModemPort,"AT+CFUN=0" + CHR(13),"0" + CHR(13),1,300) 'Clean shutdown
      Delay (1,2,sec) 'Delay to allow deregistration
      SerialClose(ModemPort)
      ModemFlag=False
    EndIf
    SW12 (ModemFlag) 'Set modem power on/off

    'Call Data Tables and Store Data
    CallTable Table_1min
    CallTable Table_15Sec
    CallTable Table_Battery
  NextScan
EndProg
