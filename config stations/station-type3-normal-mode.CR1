'W/O:  CCSF005437_3
'Customer:  JD Environnement
'Date:  16th January 2019
'Campbell Scientific Contact:  Vanessa Ben Tahar
'
'Program used to test your weather station.
'Please note the following important information:
'Use the test program as a starting point, you will need to adapt it to suit your own application.
'
'
'-Wiring For CR1000-
'
'  HC2AS3 (constant power)
'    1H:  Brown
'    1L:  Yellow
'    2H:  White
'    2L:  Link to 1L
'    G:  Grey
'    G:  Clear
'    12V:  Green
'
'  CMP3 Pyranometer (CSL)
'    3H:  Red
'    3L:  Blue
'    3L:  Link To AG
'    AG:  Clear
'
'  CGR3 Atmospheric infra-red radiation
'  Pyrgeometer
'    4H:  Red
'    4L:  Blue
'    4L:  Link to AG
'  PT100
'    5L:  Yellow (via 4WPB100)
'    AG:  Green (via 4WPB100)
'    6H:  Brown
'    6L:  Grey
'    VX1: 4WPB100 (Black)
'
'  WindSonic1 (RS-232 9.6K baud) Two Dimensional Ultrasonic Wind Sensor
'    G:  Black
'    G:  Clear
'    12V:  Red
'    C1:  Green
'    C2:  White
'
'  KalyxRG Rain Gauge (CSL)
'    P1:  Black
'    AG:  Clear
'    AG:  Screen (Yellow)
'
'-Measurement Labels-
'
'CMP3 Pyranometer sensor calibration μV/Wm^2:
Const CMP3_cal=15.02

'GPRS modem information
'Const ModemPort = ComSDC7  'This is the port to which the modem is attached, typically ComSDC7 via an SC105.
Const ModemPort = ComRS232
Const Modembaud = 115200   'Modem baud rate
Const ModemOnMinutes = 10  'Modem switched on for ModemOnMinutes every hour
Public ModemFlag As Boolean

'Declare Variables and Units
Dim Windsonic1_WSStr As String * 21
Dim Windsonic1_ByteRet
Dim Windsonic1_ChkSumF As Boolean
Dim Windsonic1_WSData(4)

Public BattV : Units BattV = Volts
Public PTemp_C : Units PTemp_C = Deg C
Public HC2AS3_AirTC : Units HC2AS3_AirTC = Deg C
Public HC2AS3_RH : Units HC2AS3_RH = %
Public CMP3_Solar_Wm2 : Units CMP3_Solar_Wm2 = W/m²
Public CMP3_Solar_MJ : Units CMP3_Solar_MJ = MJ/m²
Public CGR3_Wm2 : Units CGR3_Wm2 = W/m²
Public CGR3_Temp_C : Units CGR3_Temp_C = Deg C
Public Windsonic1_WindDir : Units Windsonic1_WindDir = degrees
Public Vitesse_vent : Units Vitesse_vent = meters/second
Public Windsonic1_WSDiag : Units Windsonic1_WSDiag = unitless
Public KalyxRG_Rain_mm :Units KalyxRG_Rain_mm = mm
Public Windsonic1_N(9)
Alias Windsonic1_N(1) = SmplsF
Alias Windsonic1_N(2) = Diag1F
Alias Windsonic1_N(3) = Diag2F
Alias Windsonic1_N(4) = Diag4F
Alias Windsonic1_N(5) = Diag8F
Alias Windsonic1_N(6) = Diag9F
Alias Windsonic1_N(7) = Diag10F
Alias Windsonic1_N(8) = NNDF
Alias Windsonic1_N(9) = CSEF

'Define Data Tables
DataTable(Table_1Min,True,-1)
  DataInterval(0,1,Min,10)
  Average(1,Vitesse_vent,FP2,False) : FieldNames("Vitesse_vent_moy")
EndTable

DataTable(Table_5Min,True,-1)
  DataInterval(0,5,Min,10)
  Average(1,BattV,FP2,False)
  Average(1,PTemp_C,FP2,False)
  Average(1,HC2AS3_AirTC,FP2,False) : FieldNames("Temp_moy")
  Average(1,HC2AS3_RH,FP2,False) : FieldNames("Hygro_moy")
  Average(1,CMP3_Solar_Wm2,FP2,False) : FieldNames("Ray_solaire_moy")
  Totalize(1,CMP3_Solar_MJ,IEEE4,False) : FieldNames("Ray_solaire_cumul")
  Average(1,CGR3_Wm2,FP2,False) : FieldNames("Ray_IR_moy")
  Average(1,CGR3_Temp_C,FP2,False) : FieldNames("Ray_CGR3_Temp_C")
  Totalize(1,KalyxRG_Rain_mm,FP2,False) : FieldNames("Pluvio_cumul")
EndTable

DataTable(Table_10Min,True,-1)
  DataInterval(0,10,Min,10)
  WindVector(1,Vitesse_vent,Windsonic1_WindDir,FP2,False,0,0,3)
  FieldNames("Direction_vent_vecteur")
  Sample(1,Windsonic1_WSDiag,FP2) : FieldNames("Vent_Diag")
EndTable

DataTable(Table_Battery,True,-1)
  DataInterval(0,5,Min,10)
  Minimum(1,BattV,FP2,False,False)
EndTable


'Main Program
BeginProg
  'In case the modem was connected when the station was reset
  PPPClose
  
  'Open Com1 for communications with the
  'WindSonic1 (RS-232 9.6K baud) Two Dimensional Ultrasonic Wind Sensor
  SerialOpen(Com1,9600,3,0,505)

  'Main Scan
  Scan(15,Sec,1,0)

    'Default CR1000 Datalogger Battery Voltage measurement 'BattV'
    Battery(BattV)
    'Default CR1000 Datalogger Wiring Panel Temperature measurement 'PTemp_C'
    PanelTemp(PTemp_C,_50Hz)

    'KalyxRG Tipping Bucket Rain Gauge measurement 'Rain_mm'
    PulseCount(KalyxRG_Rain_mm,1,1,2,0,0.2,0)

    If IfTime (0,60,Sec)
      'HC2AS3 (constant power) Temperature & Relative Humidity Sensor measurements 'AirTC' and 'RH'
      VoltDiff(HC2AS3_AirTC,1,mV2500,1,True,0,_50Hz,0.1,-40)
      VoltDiff(HC2AS3_RH,1,mV2500,2,True,0,_50Hz,0.1,0)
      If HC2AS3_RH > 100 AND HC2AS3_RH < 103 Then HC2AS3_RH = 100
    EndIf


    'CMP3 Pyranometer measurement in 'W/m²' and 'SlrMJ'
    VoltDiff(CMP3_Solar_Wm2,1,mV25,3,True,0,_50Hz,1000 / CMP3_cal,0)
    'Set negative readings to zero:
    If CMP3_Solar_Wm2 < 0 Then CMP3_Solar_Wm2 = 0
    CMP3_Solar_MJ = CMP3_Solar_Wm2 * (30 * 0.000001)

    'CGR3 Atmospheric infra-red radiation
    VoltDiff(CGR3_Wm2,1,AutoRange,4,True,0,_50Hz,17,0)
    'PT100 PRT Temperature Probe (4WPB100) (CSL) measurement 'Temp_C'
    BrHalf4W(CGR3_Temp_C,1,mV25,mV25,5,Vx1,1,2035,True,True,0,_50Hz,1,0)
    PRT(CGR3_Temp_C,1,CGR3_Temp_C,1,0)

    'WindSonic1 (RS-232 9.6K baud) Two Dimensional Ultrasonic Wind Sensor measurements 'WindDir', 'WS_ms', and 'WSDiag'
    'Get data from WindSonic1 (RS-232 9.6K baud) Two Dimensional Ultrasonic Wind Sensor
    SerialInRecord(Com1,Windsonic1_WSStr,&h02,0,&h0D0A,Windsonic1_ByteRet,01)
    SplitStr(Windsonic1_WSData(),Windsonic1_WSStr,",",4,4)
    Windsonic1_WindDir = Windsonic1_WSData(1)
    Vitesse_vent = Windsonic1_WSData(2)
    Windsonic1_WSDiag = Windsonic1_WSData(4)
    Windsonic1_ChkSumF = HexToDec(Right(Windsonic1_WSStr,2)) Eqv CheckSum(Windsonic1_WSStr,9,Len(Windsonic1_WSStr) - 3)
    'Set diagnostic variables as needed
    If Windsonic1_ByteRet = 0 Then Windsonic1_WSDiag = NAN
    Move(SmplsF,9,0,1)
    Select Case Windsonic1_WSDiag
    Case = 0
      SmplsF = 1
    Case = 1
      Diag1F = 1
    Case = 2
      Diag2F = 1
    Case = 4
      Diag4F = 1
    Case = 8
      Diag8F = 1
    Case = 9
      Diag9F = 1
    Case = 10
      Diag10F = 1
    Else
      NNDF = 1
    EndSelect
    If NOT (Windsonic1_ByteRet <> 0 IMP Windsonic1_ChkSumF) Then CSEF = 1


    'Modem control sequence
    If TimeIntoInterval (0,60,Min) Then   'Every hour turn on the modem
      ModemFlag = True 'Power on modem
      Delay (1,2,sec)
      PPPOpen
    EndIf
    If TimeIntoInterval (ModemOnMinutes,60,Min) Then  'After ModemOnMinutes turn modem off
      PPPClose
      SerialOpen(ModemPort,Modembaud,0,0,100)
      'Send the command to log off the network and shutdown cleanly, wait up to 3 sec.
      SerialOut(ModemPort,"+++","0" + CHR(13),1,150) 'Send the +++ sequence to get modem in command mode
      SerialOut(ModemPort,"AT+CFUN=0" + CHR(13),"0" + CHR(13),1,300) 'Clean shutdown
      Delay (1,2,sec) 'Delay to allow deregistration
      SerialClose(ModemPort)
      ModemFlag=False
    EndIf
    SW12 (ModemFlag)

    'Call Data Tables and Store Data
    CallTable Table_1Min
    CallTable Table_5Min
    CallTable Table_10Min
    CallTable Table_Battery

  NextScan
EndProg
