HISTORIQUE
Pour toutes Stations 1A, 1B, 1C, 1D, 1E, 2A, 2B, 3A et 3B
2019-04-11 Livraison et montage
2019-04-16 D�but essai 
2019-04-18 Fin essai
2019-05-02 D�but Etalonnage T�C et H% 
2019-05-15 Fin Etalonnage T�C et H%

Station 1A
2019-06-26 Mise en place � Climabat (ULR)
2019-08-13 Arret de la station(vol du panneau solaire). Fin climabat 
2019-08-27 Mise en place � micosol (ULR) et remise en route
2023-09-24 06:02:00 D�connexion de la station
2023-09-25 15:20:00 Intervention sur place pour reinitialiser le modem 3g et r�cup�ration des donn�es
2024-07-09 09:30:00 Arret et d�montage de la station. Station en attente d'un nouvel emplacement.

Station 1B
En attente d'emplacement

Station 1C
2019-12-09 Mise en place sur terrain gare SNCF 
2023-02-12 15:00:00 Deconnexion de la station
2023-02-28 10:00:00 Intervention sur place pour reinitialiser le modem 3g et r�cup�ration des donn�es 
2023-02-28 12:00:00 Re-deconnexion de la station
2023-04-06 10:00:00 Intervention sur place pour reinitialiser le modem 3g et r�cup�ration des donn�es

Station 1D
2019-12-17 Mise en place sur terrain Espace vert (� cot� du bureau)
2025-01-09 Remplacement de la batterie

Station 1E
2020-01-07 Mise en place sur terrain Espace vert (jardin marocain)
2020-11-08 Deconnexion de la station
2020-11-17 Intervention sur place pour reinitialiser le modem 3g et r�cup�ration des donn�es
2022-08-14 An�mom�tre bloqu� par une branche. Arr�t des donn�es de vitesse de vent � 0h00
2022-11-23 Branche enlev�e par Mr Pavy, de la mairie de La Rochelle. Reprise des donn�es de vitesse de vent � 16h45.

Station 2A
2020-01-31 D�but essai reception 3g
2020-02-14 Fin essai reception 3g
2020-06-23 Mise en place sur terrain Pont de l'ile de R� (� cot� du p�age)
2024-07-15 An�mom�tre l�g�rement gripp�, ne fonctionne pas tr�s bien par vent faible

Station 2B
2019-12-19 Mise en place sur terrain propr�t� urbaine (centre ville LR)
2020-07-09 Observation de ph�nom�ne de pics de temp�rature anormalement �lev�e
2020-07-23 D�placement de la sonde T� et H% avec son abri
2020-09-11 Remplacement de la t�te de sonde T� et H%
2021-03-26 Observation de ph�nom�ne de pics de temp�rature anormalement �lev�e
2021-06-11 D�placement de la sonde T� et H% avec son abri tout en haut du mat + suivi Tinytag sur une semaine ? retour � la normale
Finalement les observations de pics de temp�rature anormalement �lev�e continues. 
Une campagne de mesures comparative in situ est pr�vue
2022-05-05 au 2022-06-21 Campagne de mesure comparative en temp�rature et en humidit� avec 3 Tinytag ext�rieur (T� et H%) ? Observation d��carts de temp�rature tr�s importants entre la sonde de la station 2B et les tinytag, principalement lorsque les temp�ratures sont �lev�es.
2022-10-04 D�montage de la station
Fin des donn�es pour 2B le 04/10/2022 � 14h00 (remplac�e par 2C)

Station 3A
2019-05-27 Mise en place sur terrain priv� chez Karim (Usseau - Sainte Soulle)
2020-08-12 D�connexion de la station (Orage?)
2020-08-25 Remise en route de la station

Station 3B
2019-07-17 Mise en place cour int�rieure du b�timent Pascal (ULR)
2019-12-04 Ajout d'une sonde pt100 boule noire "Temp_rad"

Station 2C
2022-09-23 Etalonnage en T� et H%
2022-10-06 Mise en place sur terrain propr�t� urbaine (centre ville LR) en remplacement de 2B

Station 2D
2023-05-11 Etalonnage en T�
2024-12-04 Mise en place sur toiture du b�timent MSI (ULR)

Station 3C
2024-09-26 Etalonnage en T�
2025-02-18 Mise en place sur terrain marais cnrs � l'Houmeau