# Données des stations
- Temp_moy : température d'air °C
- Hygro_moy : hygrométrie de l'air %
- Vitesse_vent_moy : vitesse du vent m/s
- Direction_vent_vecteur : orientation du vent °
- Ray_solaire_moy : éclairement solaire sur l'horizontale W/m²
- Ray_IR_moy : éclairement grande longueur d'onde sur l'horizontale W/m²
- Pluvio_cumul : cumul de pluviométrie mm par 5min
- T_rad : température radiante de boule noire °C
- autres données : températures de batteries et autres capteurs

## EQLORE - urban weather station data - la Rochelle (France)

- weather station network: 1 rural (10 km from the city) and 7 urban weather stations in La Rochelle city
- réseau de stations météo : 1 station météo rurale (à 10 km de la ville) et 7 stations météo urbaines dans la ville de La Rochelle

### DATA
- See ./archives/ (in subfolders) for all weather station data (updated monthly) in CSV files (data grouped by year)
- Voir ./archives/ (dans les sous-dossiers) pour toutes les données des stations météorologiques (mises à jour mensuellement) dans des fichiers CSV (données regroupées par an)

### REFERENCE TO CITE WHEN USING DATA
Martinez, S., A. Machard, A. Pellegrino, K. Touili, L. Servant, et E. Bozonnet. « A Practical Approach to the Evaluation of Local Urban Overheating– A Coastal City Case-Study ». Energy and Buildings 253 (15 décembre 2021): 111522. https://doi.org/10.1016/j.enbuild.2021.111522.

### Locations and periods
- See ./config-station.xlsx for all weather station locations (lat,lon) and monitoring periods
- Voir ./config-station.xlsx pour tous les emplacements des stations météorologiques (lat, lon) et les périodes de mesure

# Procédure de mise à jour des données (chaque mois)

### Mettre le dossier local à jour avec la version en ligne
1. ouvrir le dossier `C:/Users/lserva03/Documents/eqlore_uws` 
2. dans la barre d'adresse de l'explorateur, taper `cmd` + `Enter`
3. dans l'invite de commande, taper `git stash` + `Enter`, dans l'invite de commande un message commençant par  `Saved working directory...` apparaît
3. dans l'invite de commande, taper `git merge --strategy-option theirs` + `Enter`, si l'on souhaite repartir de la version locale, remplacer `theirs` par `ours` 

### Récupérer les données de GitLab
1. ouvrir le dossier `C:/Users/lserva03/Documents/eqlore_uws` 
2. dans la barre d'adresse de l'explorateur, taper `cmd` + `Enter`
3. dans l'invite de commande, taper `git pull` + `Enter`, si le programme a été modifié, la MAJ se fait

### Mettre à jour les évènements
1. mettre à jour le document `Historique des stations.txt` se trouvant dans `Z:/`
2. mettre à jour la feuille excel `config-stations.xlsx` avec la localisation et les sensibilités des pyrano et pyrgéo des stations se trouvant dans le dossier `C:/Users/lserva03/Documents/eqlore_uws/`
- 2.1 dupliquer la ligne de la station arrêtée/déplacée ou nouvellement mises en route
- 2.2 renommer la station arrêtée/déplacée avec l'année d'arrêt (ex : `Station-2B_2022`)
- 2.3 dans la colonne `fin` de la station arrêtée mettre la date d'arrêt avec un jour de **moins**
- 2.4 dans la ligne dupliquée de la station sans l'année à la fin (ex : `Station-2B`), modifier la daté de début avec un jour de **plus**
- 2.5 pour cette ligne dupliquée, renseigner la nouvelle localisation (vide pour le stockage)

### Mettre à jour les données météo
1. dans `Z:/`, sélectionner tous les fichiers avec `Ctrl+A` puis les Copier/Coller dans le dossier `C:/Users/lserva03/Documents/eqlore_uws/data`
2. dans l'invite de commande précédente, taper `git add .` + `Enter`
3. dans l'invite de commande précédente, taper `git commit -m "ADDED: data station 24/08/22"` + `Enter` (remplacer la date)
4. dans l'invite de commande précédente, taper `git push`
5. vérifier à l'adresse https://gitlab.univ-lr.fr/ebozonne/eqlore_uws/-/tree/main/data que les dernières données sont en ligne



