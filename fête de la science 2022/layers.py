import pandas as pd
from requests import get
import geojson
import numpy as np
import json
from OSMPythonTools.overpass import overpassQueryBuilder, Overpass
import geopandas as gpd
import warnings
import rasterio
import rioxarray
from osgeo import ogr, gdal
import os
from subprocess import Popen
import shutil
warnings.filterwarnings("ignore")

class IGNcollect():
    '''
    ===
    Classe qui permet
    - de construire une reqûete pour interroger l'API de l'IGN
    - enregistre les données dans le dossier ./demo/
    ===
    '''
    def __init__(self, name = 'buildings', key = 'BD TOPO® V3 batiment', mask = './mon-quartier/mask.geojson'):
        with open(mask) as f:
            self.gj = geojson.load(f)
        self.name = name
        self.coords = np.array(list(geojson.utils.coords(self.gj)))
        self.bbox = [ self.coords[:,0].min(),self.coords[:,1].min(),  self.coords[:,0].max(), self.coords[:,1].max()]
        self.bbox_tif = [self.coords[:, 1].min(), self.coords[:, 0].min(), self.coords[:, 1].max(), self.coords[:, 0].max()]
        # self.bbox = get_bounding_box(mask)
        self.csv_file = pd.read_csv('./demo/Ressources_de_services_web_2022-05-20.csv',
                            encoding = 'ISO-8859-1', sep = ';', index_col=2, header = 0)
        row = self.csv_file.loc[(self.csv_file.index == key)]
        nom = row['Nom technique'].values[0]
        requete = row["URL d'accès"].values[0].split('&REQUEST=GetCapabilities')[0]

        self.request =  requete+ \
                      f'&REQUEST=GetFeature&TYPENAME={nom}&SRSNAME=EPSG:4326' \
                      f'&BBOX={round(self.bbox[0],5)},{round(self.bbox[1],5)},{round(self.bbox[2],5)},{round(self.bbox[3],5)},' \
                      'EPSG:4326&STARTINDEX=0&COUNT=10000&outputFormat=application/json&SERVICE=WFS'

        self.request_tiff = requete+\
        f'&LAYERS={nom}&' \
        'EXCEPTIONS=text/xml&FORMAT=image/geotiff&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap' \
        '&STYLES=&CRS=EPSG:4326' \
        f'&BBOX={round(self.bbox_tif[0],5)},{round(self.bbox_tif[1],5)},' \
                            f'{round(self.bbox_tif[2],5)},{round(self.bbox_tif[3],5)}' \
        '&WIDTH=1000&HEIGHT=1000&DPI=25'
    def run(self):
        print(self.request)
        r = get(self.request)
        open(f'./mon-quartier/{self.name}.geojson', 'wb').write(r.content)

        r = get(self.request_tiff)
        print(self.request_tiff)
        open(f'./mon-quartier/{self.name}.tiff', 'wb').write(r.content)


class OSMcollect():
    '''
    ===
    Classe qui permet
    - de construire une reqûete pour interroger l'API de OpenStreetMap
    - enregistre les données dans le dossier ./demo/
    ===
    '''
    def __init__(self, name = 'tree', key = '"natural"="tree"', mask = './mon-quartier/mask.geojson'):
        with open(mask) as f:
            self.gj = geojson.load(f)
        self.name = name
        self.key = key
        self.coords = np.array(list(geojson.utils.coords(self.gj)))
        self.bbox = [self.coords[:,0].min(),self.coords[:,1].min(),
                     self.coords[:,0].max(), self.coords[:,1].max()]


    def run(self):
        overpass = Overpass()
        query = overpassQueryBuilder(bbox=[self.bbox[1], self.bbox[0], self.bbox[3], self.bbox[2]],
            elementType=['way', 'relation', 'node'],
            selector=self.key,
            includeGeometry=True)
        print(self.bbox)
        print(self.key)
        print(query)
        result = overpass.query(query)
        result.countElements()
        jsonList = result.elements()
        geojsonEmpty = {"type": "FeatureCollection",
                        "name": "OSMPythonTools",
                        "features": []
                        }
        for k, item in enumerate(jsonList):
            toAdd = {
                "type": "Feature",
                "geometry": {
                    "type": item.geometry()['type'],
                    "coordinates": item.geometry()['coordinates']
                },
                "properties": {
                    "name": "{}{}".format(self.key , k)
                }
            }
            geojsonEmpty['features'].append(toAdd)
        with open(f'./mon-quartier/{self.name}.geojson', 'w') as file:
            json.dump(geojsonEmpty, file)

def geojsonToSHP(file = './demo/buildings.geojson'):
        name = file.split('\\')[-1].split('.geojson')[0]
        gdf = gpd.read_file(file)
        gdf.to_file('./mon-quartier/' + name + '.shp')


def layerVegetation(image_path = './mon-quartier/irc.tiff',
                    outfile = './mon-quartier/ndvi.shp'):
    dataset = rasterio.open(image_path)
    dataset_rio = rioxarray.open_rasterio(image_path)
    image = dataset.read()
    np.seterr(divide='ignore', invalid='ignore')
    bandNIR = image[0, :, :]
    bandRed = image[1, :, :]
    ndvi = (bandNIR.astype(float) - bandRed.astype(float)) / (bandNIR.astype(float) + bandRed.astype(float))
    filter_raster = []
    for x in ndvi:
        filter_raster.append([-999 if y<0.2 else y for y in x])
    dataset_rio.data[0] = filter_raster
    dataset_rio.rio.to_raster('./mon-quartier/ndvi.tif', tiled=True)

    #================================================
    image_path = './mon-quartier/ndvi.tif'
    # open image:
    im = gdal.Open(image_path)
    srcband = im.GetRasterBand(1)

    if srcband.GetNoDataValue() is None:
        mask = None
    else:
        mask = srcband

    # create output vector:
    driver = ogr.GetDriverByName('ESRI Shapefile')

    if os.path.exists(outfile):
        driver.DeleteDataSource(outfile)

    vector = driver.CreateDataSource(outfile)
    layer = vector.CreateLayer(outfile.replace('.shp', ''), im.GetSpatialRef(), geom_type=ogr.wkbPolygon)

    # create field to write NDVI values:
    field = ogr.FieldDefn('NDVI', ogr.OFTReal)
    layer.CreateField(field)
    del field

    # polygonize:
    gdal.Polygonize(srcband, mask, layer, 0, options=[], callback=gdal.TermProgress_nocb)

    # close files:
    del im, srcband, vector, layer

def getGeoclimate_osm(name = 'geoclimate', mask = './mon-quartier/mask.geojson'):
    with open(mask) as f:
        gj = geojson.load(f)
    name = name
    coords = np.array(list(geojson.utils.coords(gj)))
    bbox = [coords[:, 0].min(), coords[:, 1].min(), coords[:, 0].max(), coords[:, 1].max()]
    # print([[bbox[1], bbox[0], bbox[3], bbox[2]]])
    print(bbox)
    pathJson = './geoclimate/my_first_config_file_osm.json'

    with open(pathJson) as f:
        myJson = json.load(f)
        myJson['input']['locations'] = [[bbox[1], bbox[0], bbox[3], bbox[2]]]
        myJson['output']['folder'] = './mon-quartier/'
        try:
            os.mkdir(myJson['output']['folder'])
        except:
            pass
        with open('./geoclimate/toGo.json', 'w') as f:
            json.dump(myJson, f)
            f.close()
        commands = "java -jar ./geoclimate/geoclimate-0.0.2.jar -w OSM -f ./geoclimate/toGo.json"
    Popen(commands).wait()