import folium
import branca.colormap as cmp
import json
from shapely.geometry import Polygon, Point
import geopandas as gpd
import pandas as pd
import seaborn as sns
#traitement des données
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pickle
import plotly.express as px
import matplotlib.pyplot as plt

def displayMap():
    m = folium.Map(location=[46.155821282320524, -1.153276741101409], tiles='Stamen Toner',  zoom_start=12)
    data = "./carto-data/merge.geojson"
    data = "./LR/rsu_lcz.geojson"
    with open(data) as f:
        myGeoJsonClimate = json.load(f)

    tableCorresp = {   
        1 :['LCZ 1: Compact high-rise', '#8b0101'],
        2 :['LCZ 2: Compact mid-rise', '#cc0200'],
        3 :['LCZ 3: Compact low-rise', '#fc0001'],
        4 :['LCZ 4: Open high-rise', '#be4c03'],
        5 :['LCZ 5: Open mid-rise', '#ff6602'],
        6 :['LCZ 6: Open low-rise', '#ff9856'],
        7 :['LCZ 7: Lightweight low-rise', '#fbed08'],
        8 :['LCZ 8: Large low-rise', '#bcbcba'],
        9 :['LCZ 9: Sparsely built', '#ffcca7'],
        10 :['LCZ 10: Heavy industry', '#57555a'],
        101 :['LCZ A: Dense trees', '#006700'],
        102 :['LCZ B: Scattered trees', '#05aa05'],
        103 :['LCZ C: Bush,scrub', '#648423'],
        104 :['LCZ D: Low plants', '#bbdb7a'],
        105 :['LCZ E: Bare rock or paved', '#010101'],
        106 :['LCZ F: Bare soil or sand', '#fdf6ae'],
        107 :['LCZ G: Water', '#6d67fd']
    }
    for k, item in enumerate(myGeoJsonClimate['features']):
        try:myGeoJsonClimate['features'][k]['properties']['legend'] = tableCorresp[myGeoJsonClimate['features'][k]['properties']['LCZ_PRIMARY']][0]
        except:myGeoJsonClimate['features'][k]['properties']['legend'] = tableCorresp[int(str(myGeoJsonClimate['features'][k]['properties']['LCZ_PRIMARY']).replace('0',''))][0]
        
    for x in myGeoJsonClimate['features']:
        x['LCZ_PRIMARY'] = x['properties']['LCZ_PRIMARY']
    #     if x['properties']['LCZ_PRIMARY'] in [101, 102, 103, 104, 105, 106, 107]:
    #         x['LCZ_PRIMARY'] = int(str(x['properties']['LCZ_PRIMARY']).replace('0',''))
    #     else:
    #         x['LCZ_PRIMARY'] = x['properties']['LCZ_PRIMARY']

    step = cmp.StepColormap(
     [tableCorresp[k][1] for k in tableCorresp.keys()],
     vmin=0, vmax=18,
     index=tableCorresp.keys(),  #for change in the colors, not used fr linear
     caption='Color Scale for Map'    #Caption for Color scale or Legend
    )


    folium.GeoJson(
            myGeoJsonClimate,
            style_function=lambda feature: {
                'fillColor': step(feature['LCZ_PRIMARY']),
                'color': 'black',
                'weight': 0.5,
                'alpha': 1,
                "fillOpacity": 0.35},
            #         'dashArray' : '5, 5'
            tooltip=folium.features.GeoJsonTooltip(
                fields=['legend'],
                aliases=[''],
            )

        ).add_to(m)

    title_html = '''
                 <h3 align="center" style="font-size:20px"><b>Cartographie - LCZ La Rochelle</b></h3>
                 '''
    m.get_root().html.add_child(folium.Element(title_html))

    return m
    
    
    
def reseauStation(m):

    PositionStations = {'Station-1A' : [-1.158845984057876, 46.14496400302767], 
                        'Station-1B' : [],
                        'Station-1C' :[-1.146211345412327, 46.15113874727135], 
                        'Station-1D' :[-1.155870285076597, 46.16869150693134],
                        'Station-1E' :[-1.155348489467828, 46.168147091533314],
                        'Station-2A' :[-1.219480000000000, 46.175516000000000],
                         'Station-2B' :[-1.148074409034999, 46.167916208017544],
                        'Station-2C' :[],
                        'Station-2D' :[],
                        'Station-3A' :[-1.030987516728151, 46.203311177306006],
                        'Station-3B' :[-1.157541323896073, 46.14649249701438],
                        'Station-3C' :[],
                        'Station-3D' :[]
                       }
    geom = []
    nom_station = []
    lat = []
    lon = []
    for name  in PositionStations.keys():
        try:
            geom.append(Point(PositionStations[name]))
            nom_station.append(name)
            lat.append(PositionStations[name][0])
            lon.append(PositionStations[name][1])
        except:pass
    stations = gpd.GeoDataFrame(index = nom_station, crs='epsg:2154', geometry=geom)
    stations['lat'] = lat
    stations['lon'] = lon
    # m = folium.Map(location=[46.155821282320524, -1.153276741101409], tiles='Stamen Toner',  zoom_start=12)
    for index in stations.index:
        toplot = stations[stations.index == index]
        folium.Marker(
        location=[toplot['lon'], toplot['lat']],
        popup=index,
        icon=folium.Icon(icon="screenshot")
        ).add_to(m)
        
        
    return m, stations
    
def correctionData(listDesStations, dataStations):
    #----------------------------------------------------
    #  modification des données solaires
    #----------------------------------------------------
    tableCorrespondance = {
        'Vitesse_vent_moy' : '1min',
        'BattV_Avg' : '5min',
        'PTemp_C_Avg' : '5min',
        'Temp_moy' : '5min',
        'Hygro_moy' : '5min',
        'BattV_Min' : 'Battery',
        'Ray_solaire_moy' : '5min',
        'Ray_solaire_cumul' : '5min',
        'Ray_IR_moy' : '5min',
        'Ray_CGR3_Temp_C' : '5min',
        'Pluvio_cumul' : '5min',
        'Direction_vent_vecteur' : '10min', 
        'Vent_Diag' : '10Min'
    }
    config = pd.read_excel('config-stations.xlsx', index_col=0) 
    for station in listDesStations:
        try:
            CMP3 = dataStations[station][tableCorrespondance['Ray_solaire_moy']]['Ray_solaire_moy']
            dataStations[station][tableCorrespondance['Ray_solaire_moy']]['Ray_solaire_moy'] = CMP3*(15.02/config['CMP3'][station])
        except:pass
        try:
            CGR3 = dataStations[station][tableCorrespondance['Ray_IR_moy']]['Ray_IR_moy']
            dataStations[station][tableCorrespondance['Ray_IR_moy']]['Ray_IR_moy']= CGR3*((1000/config['CGR3'][station])/17)
        except:pass
    return dataStations, tableCorrespondance
    
    
def f(dt, nbrOfInterval = 10., dtMax = 6):
    import seaborn
    import numpy as np
    dT = dtMax/nbrOfInterval
    listBorne = np.arange(-dtMax, dtMax+dT,dT)
    listBorne_1 = [x-dT for x in listBorne]
    listOfInterval = [[x, y] for (x,y) in zip(listBorne_1, listBorne)]
#     palette = reversed(seaborn.color_palette(palette= "coolwarm", n_colors= len(listOfInterval), desat= 1).as_hex())
    palette = sns.diverging_palette(240, 10, l=50, n=len(listOfInterval)).as_hex()
    N = len(listOfInterval)
    listOfSize = [(abs(x)+2)*4 for x in listBorne]
    
    for interval, col, size in zip(listOfInterval, palette, listOfSize):
        if dt > interval[0] and dt<=interval[1]:
            if interval[0]>0:
                color = palette[0]
            else:
                color = palette[-1]
            markerSize = size
        else:pass
    return color, markerSize
    
def dataMap(debut, stations, dataStations, tableCorrespondance):
    #----------------------------------------------------
    #  intervalle de calcul pour la moyenne
    #----------------------------------------------------
    nbr = 5/60.
    fin = (debut + timedelta(hours=nbr)).strftime("%Y-%m-%d %H:%M:%S")
    debutTitle = debut.strftime("%Y-%m-%d %H:%M")
    debut = debut.strftime("%Y-%m-%d %H:%M:%S")

    dataMap = {}
    for name in stations.index:
        dataMap[name] = {}
        dataMap[name]['temp'] = round(dataStations[name][tableCorrespondance['Temp_moy']][debut:fin].Temp_moy.mean(),1)

    deltaT = []
    temp = []
    listeColor = []
    listeSize = []
    for name in stations.index:
        dataMap[name]['dt'] = round(dataMap['Station-3A']['temp']-round(dataStations[name][tableCorrespondance['Temp_moy']][debut:fin].Temp_moy.mean(),1),3)
        deltaT.append(dataMap[name]['dt'])
        temp.append(dataMap[name]['temp'])
    #     print(dataMap[name]['dt'])
        color, size = f(dataMap[name]['dt'])
        listeColor.append(color)
        listeSize.append(size*30)
    stations['dt'] = deltaT
    stations['temp'] = temp 
    stations['color'] = listeColor 
    stations['size'] = listeSize 
    stations.loc[stations.index == 'Station-3A', 'size'] = 100
    stations.loc[stations.index == 'Station-3A', 'color'] = 'black'
    
    return stations, dataMap
    
def mapReseauStation(stations, dataMap, debut):
    m = folium.Map(location=[46.155821282320524, -1.153276741101409], tiles='Stamen Toner',  zoom_start=12)
    for index in stations.index:
        toplot = stations[stations.index == index]
        if index == 'Station-1E':
            toplot['lon'] = toplot['lon']+0.003
        else:pass
        
        folium.Circle(
        radius=toplot['size'].values[0]*0.8,
        location=[toplot['lon'], toplot['lat']],
            popup=folium.Popup(str(toplot.index.values[0])+' <br>'+str(toplot['temp'].values[0])+'°C'+' <br>', 
                         parse_html=False, max_width=1000),
        #popup=str(toplot.index.values[0])+' <br>'+str(toplot['temp'].values[0])+'°C'+' <br>',
        color='black',
            weight = 1,
        fill=True,
            opacity=0.9,
        fill_opacity=0.9,
        fill_color=toplot['color'].values[0],
        ).add_to(m)
    title_html = f'''
                 <h3 align="center" style="font-size:20px"><b>Trurale = {str(dataMap['Station-3A']['temp'])} °C</b>
                 <br>{debut}</br></h3>
                 '''
    m.get_root().html.add_child(folium.Element(title_html))
    return(m)
    
    
def pluviometrie(dataStations, station, i, e):
    try:
        sns.set_theme(style="white",font_scale=2)

        cmap = plt.cm.Blues
        rainData = dataStations[station]['5min']['Pluvio_cumul'][i:e].resample('1h').sum()
        df = pd.DataFrame()
        df['rain']=[x for x in dataStations[station]['5min']['Pluvio_cumul'][i:e].resample('1h').sum()]
        df.index = pd.to_datetime(rainData.index)
        df['heures'] = df.index.hour
        df['days'] = pd.to_datetime(df.index.date)
        df.rain=df.rain #convert in mm
        rainPerDay = pd.pivot_table(df, values="rain", index=["heures"], columns=["days"])
        rainMinMax = pd.pivot_table(df, values="rain", index=["days"], aggfunc= [sum, min, max])


        f, (ax0, ax1) = plt.subplots(2, 1, figsize=(20, 10), gridspec_kw = {'height_ratios':[2, 2], 'hspace':0})
        rainMinMax['sum'].plot(ax=ax0,title="rain mm/day",kind='bar',sharex=True,legend=False,edgecolor=cmap(200),color=cmap(200),snap=False)
        sns.heatmap(rainPerDay, xticklabels=30, yticklabels=6, ax=ax1, cmap=cmap,
                    cbar_ax=f.add_axes([.92,.13,.01,.5]), cbar_kws={'use_gridspec':True,
                                                                    'label': "rain [mm]"})
        #mise en forme finale:
        ax1.invert_yaxis()
        ax1.xaxis.set_ticklabels([d.strftime('%d-%b') for d in rainPerDay.columns[ax1.get_xticks().astype(int)]])
    except:print('Pas de données pour cette station')
    
    
def temperature(dataStations, station, i, e):
    try:
        sns.set_theme(style="white",font_scale=2)
        cmap = plt.cm.coolwarm
        tempData = dataStations[station]['5min']['Temp_moy'][i:e].resample('1h').mean()
        df = pd.DataFrame()
        df['temp']=[x for x in dataStations[station]['5min']['Temp_moy'][i:e].resample('1h').mean()]
        df.index = pd.to_datetime(tempData.index)
        df['heures'] = df.index.hour
        df['days'] = pd.to_datetime(df.index.date)
        dailyTemp = pd.pivot_table(df, values="temp", index=["heures"], columns=["days"])


        f, (ax1) = plt.subplots(1, 1, figsize=(20, 6), gridspec_kw = {'height_ratios':[1], 'hspace':0})
        sns.heatmap(dailyTemp, xticklabels=30, yticklabels=6, ax=ax1, cmap=cmap, center = 20,
                    cbar_ax=f.add_axes([.92,.14,.01,.7]), cbar_kws={'use_gridspec':True,
                                                                    'label': "Temp [°C]"})
        ax1.invert_yaxis()
        ax1.xaxis.set_ticklabels([d.strftime('%d-%b') for d in dailyTemp.columns[ax1.get_xticks().astype(int)]])
    except:print('Pas de données pour cette station')
        
def ensoleillement(dataStations, station, i, e):
    try:
        # ensoleillement
        cmap = plt.cm.viridis
        solarData = dataStations[station]['5min']['Ray_solaire_moy'][i:e].resample('60min').mean()
        df = pd.DataFrame()
        df['temp']=[x for x in dataStations[station]['5min']['Ray_solaire_moy'][i:e].resample('60min').mean()]
        df.index = pd.to_datetime(solarData.index)
        df['heures'] = df.index.hour
        df['days'] = pd.to_datetime(df.index.date)
        dailySolar = pd.pivot_table(df, values="temp", index=["heures"], columns=["days"])


        f, (ax1) = plt.subplots(1, 1, figsize=(20, 6), gridspec_kw = {'height_ratios':[1], 'hspace':0})
        sns.heatmap(dailySolar, xticklabels=30, yticklabels=6, ax=ax1, cmap=cmap,
                    cbar_ax=f.add_axes([.92,.13,.01,.7]), cbar_kws={'use_gridspec':True,
                                                                    'label': "Ray_solaire_moy [W/m2]"})
        #mise en forme finale:
        ax1.invert_yaxis()
        ax1.xaxis.set_ticklabels([d.strftime('%d-%b') for d in dailySolar.columns[ax1.get_xticks().astype(int)]])
        f.show()
    except:print('Pas de données pour cette station')
    

    

def roseDesVents(dataStations, station, i, e):    

    def degToCompass(num):
        val = int((num / 22.5) + .5)
        arr = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
        return (arr[(val % 16)])


    def borne(serie, bornesmoins=[0, 1, 2, 3, 4, 5], bornesplus=[1, 2, 3, 4, 5, 6],
              cat=['0-1', '1-2', '2-3', '3-4', '4-5', '5-6']):
        liste = []
        for val in serie.values:
            for k in range(6):
                toAdd = '6+'
                if bornesmoins[k] <= val < bornesplus[k]:
                    toAdd = cat[k]
                    break
            liste.append(toAdd)
        return liste


    def borne_adapt(serie, bornesmoins=list(range(0, int(weather['Wind Speed'].max())+2, 2)), 
                    bornesplus='a calculer'):
        bornesplus = list(range(0, int(weather['Wind Speed'].max())+2, 2))[1:]
        bornesplus.append(list(range(0, int(weather['Wind Speed'].max())+2, 2))[-1]+2)
        liste = []
        for val in serie.values:
            for k in range(len(bornesmoins)):
                if bornesmoins[k] <= val < bornesplus[k]:
                    toAdd = str(bornesmoins[k])+ '-'+str(bornesplus[k])
                    break
            liste.append(toAdd)
        return liste

    def corresp_bornes(bornesmoins=list(range(0, int(weather['Wind Speed'].max())+2, 2)), 
                    bornesplus="a calculer"):
        bornesplus = list(range(0, int(weather['Wind Speed'].max())+2, 2))[1:]
        bornesplus.append(list(range(0, int(weather['Wind Speed'].max())+2, 2))[-1]+2)
        dico = {}
        for k in range(len(bornesmoins)):
            toAdd = str(bornesmoins[k])+ '-'+str(bornesplus[k])
            dico[toAdd] = k*100
        return dico
    
    
    weather = pd.DataFrame()
    windSpeed = dataStations[station]['1min']['Vitesse_vent_moy'][i:e].resample('60min').mean()*3.6
    windDir = dataStations[station]['10Min']['Direction_vent_vecteur'][i:e].resample('60min').mean()


    # windSpeed = weather['Wind Speed'].values
    # windDir = weather['Wind Direction'].values
    weather['Wind Direction'] = [x for x in windDir]
    weather['Wind Speed'] = [x for x in windSpeed]
    weather = weather.dropna() 
    # weather = weather.dropna()
    listDir = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
    directionVentcorresp = {}
    for k, d in enumerate(listDir):
        directionVentcorresp[d] = k
    correspBornes = corresp_bornes()  


    data = pd.DataFrame()
    data['dir'] = [degToCompass(x) for x in weather['Wind Direction']]
    data['sens'] = [x for x in weather['Wind Direction']]
    data['strenght'] = borne_adapt(weather['Wind Speed'])
    data['name'] = data['dir'] + '_' + data['strenght']
    data['speed'] = weather['Wind Speed'].values

    listeFreq = []
    listeDir = []
    listeStrenght = []
    listeSens = []


    #test amélioration
    for name in list(dict.fromkeys(data['name'])):
        freq = 100 * len(data.loc[(data['name']==name)]) / len(weather)
        listeFreq.append(round(freq, 3))
        listeDir.append(name.split('_')[0])
        listeStrenght.append(name.split('_')[1])
    df = pd.DataFrame()
    # Laborieux > à optimiser
    #-----------------------------------------
    df["frequency1"] = listeFreq
    df["direction1"] = listeDir
    df["m/s"] = listeStrenght
    df['name1'] = df["direction1"] + '_' + df['m/s']
    order = [int(x[0][0]) for x in df["m/s"]]
    df['order1'] = order
    # df1 = df1.sort_values(by=['order1'])
    # df.index = df1['name1']
    #result = pd.concat([df, df1], axis=1)
    #"plotly", "plotly_white", "plotly_dark", "ggplot2", "seaborn", "simple_white"


    df['1'] = [directionVentcorresp[x] for x in df['direction1']]
    df['2'] = [correspBornes[x] for x in df['m/s']]
    df['order'] = df['1']+df['2']
    df = df.sort_values(by=['order'])



    fig = px.bar_polar(df, r="frequency1", theta="direction1",
        color="m/s", template="none",
        color_discrete_sequence=px.colors.sequential.RdBu_r,
        title = station)
    #     fig.write_image("windrose.svg")
    fig.show()
    
def surchauffe(seuil, dataStations, station, i, e):   
    cmap = plt.cm.coolwarm
    tempData = dataStations[station]['5min']['Temp_moy'][i:e].resample('1h').mean()
    df = pd.DataFrame()
    df['temp']=[x for x in dataStations[station]['5min']['Temp_moy'][i:e].resample('1h').mean()]
    df.index = pd.to_datetime(tempData.index)
    df['heures'] = df.index.hour
    df['days'] = pd.to_datetime(df.index.date)
    dailyTemp = pd.pivot_table(df, values="temp", index=["heures"], columns=["days"])

    df['surchauffe'] = (df['temp']-seuil).apply(lambda x:max(x,0))

    tempPerDay = pd.pivot_table(df, values="temp", index=["heures"], columns=["days"])
    tempMinMax = pd.pivot_table(df, values="surchauffe", index=["days"], aggfunc= [sum, min, max, lambda x:sum(x>0)])


    f, (ax01,ax02, ax1) = plt.subplots(3, 1, figsize=(20, 10), gridspec_kw = {'height_ratios':[1,1, 2], 'hspace':0})
    tempMinMax['<lambda>'].plot(ax=ax01,title="Temp °C",kind='bar',sharex=True,legend=False,edgecolor=cmap(200),color=cmap(200),snap=False)
    tempMinMax['sum'].plot(ax=ax02,kind='bar',sharex=True,legend=False,edgecolor=cmap(200),color=cmap(200),snap=False)

    sns.heatmap(tempPerDay, xticklabels=30, yticklabels=6, ax=ax1, cmap=cmap,
                cbar_ax=f.add_axes([.92,.13,.01,.4]), cbar_kws={'use_gridspec':True,
                                                                'label': "Temp [°C]"})
    #mise en forme finale:

    #TODO : ajouter nbr d'heures
    ax01.set_ylabel(u"Nbr\n d'heures")
    ax02.set_ylabel(u"°CH de\n surchauffe")
    ax1.invert_yaxis()
    ax1.xaxis.set_ticklabels([d.strftime('%d-%b') for d in tempPerDay.columns[ax1.get_xticks().astype(int)]])
    plt.show()